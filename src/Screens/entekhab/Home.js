import React from "react";
import heroEntekhab from '../../assets/main pic.jpg'
import heroStart from '../../assets/hero-start.png'
import {Link} from "react-router-dom";
import SamandehiLogo from "../../Components/Samandehi/SamandehiLogo";
import EnamadLogo from "../../Components/Samandehi/EnamadLogo";
import analyze from '../../assets/analyze.png'
import dice from '../../assets/dice.png'
import dice2 from '../../assets/dice2.png'
import search from '../../assets/search.png'
import priority from '../../assets/priority.png'
import priority2 from '../../assets/priority2.png'
import routes from "./routes";

export default function Home({dispatch,getUrl,group,year}){
    console.log(group);
    return <div>
        <div
            className="hero d-flex align-items-center ">
            <div className="container p-lg-1 pt-5 d-flex flex-column flex-lg-row align-items-center">
                <div className="col-12 col-lg-6 d-flex flex-column justify-content-center align-content-center my-3 h-100">
                    <h2 className="hero-title">مشاوران تحصیلی</h2>
                    <h5 className="mt-3"> <b>نرم افزار انتخاب رشته وزارت
                        {group===1 ?' بهداشت ':' علوم '}
                        {year}
                        
                        </b>
                        </h5>
                    <h5 className="mt-3">تحلیل دقیق شانس قبولی در همه رشته محل ها + چیدمان بی نظیر و بی نقص انتخاب رشته</h5>
                    <div className=" my-4">
                        <Link to={getUrl(routes.startWithoutCode)} className="btn btn-primary">شروع کنید</Link>
                        <p className={'m-0 my-3'}>یا</p>
                        <Link to={getUrl(routes.startWithCode)}  className="btn btn-secondary">با کد اختصاصی که قبلا از نرم افزار دریافت کرده اید وارد شوید</Link>
                    </div>
                </div>
                <div className="col-12 col-lg-6 d-flex flex-column align-items-center justify-content-center h-100 w-100">
                    <h5 style={{fontSize: 18}}><b>قبولی در بهترین گرایش و معتبرترین دانشگاهی که رتبه شما اجازه میدهد</b></h5>
                    <img src={heroEntekhab} className="w-100 h-80" alt=""/>
                </div>
            </div>
        </div>
        <div className="container p-lg-5">
            {group===1?
            <>
                <h2 className="my-5 text-center w-100">مناسب ترین نرم افزار در انتخاب رشته ارشد وزارت بهداشت</h2>
                <p className="lineHeight">نرم افزار انتخاب رشته ارشد وزارت بهداشت 1403 مرکز مشاوران تحصیلی حاصل یک سال تحلیل دقیق مشاوران متخصص انتخاب رشته در مقطع کارشناسی ارشد علوم پزشکی است. بیش از 150 مشاور، تک تک کدرشته های دفترچه انتخاب رشته کارشناسی ارشد وزارت بهداشت را تحلیل کرده اند و براساس اطلاعات کارنامه نهایی (کارنامه سبز) داوطلبان در سنوات گذشته آخرین رتبه قبولی را تعیین نموده اند تا شما همه شانس های قبولی خود را در تمام رشته محل های روزانه و شهریه پرداز مشاهده و بررسی نمایید. دقت نرم افزار براساس بررسی های قبولی سال گذشته بیش از 98 درصد ارزیابی شده است.</p>
                <p className="lineHeight">اما این پایان کار ما نبوده است. سخت ترین، مهم ترین و حساس ترین مرحله انتخاب رشته ارشد وزارت بهداشت، اولویت بندی دقیق و صحیح کدرشته های دفترچه انتخاب رشته ارشد وزارت بهداشت است. آنچه که در اولویت بندی اهمیت دارد، این است که براساس رتبه خود بهترین گرایش و معتبرترین دانشگاه را قبول شویم. آنچه که ما در اولویت بندی بی نظیر انتخاب رشته ارشد وزارت بهداشت مشاوران تحصیلی به شما ارائه می کنیم، بهترین قبولی براساس رتبه شما را رقم خواهد زد.</p>
            </>
            :
            <>
                <h2 className="my-5 text-center w-100">مناسب ترین نرم افزار در انتخاب رشته ارشد</h2>
                <p className="lineHeight">نرم افزار انتخاب رشته ارشد 1403 مرکز مشاوران تحصیلی حاصل یک سال تحلیل دقیق مشاوران متخصص انتخاب رشته در مقطع کارشناسی ارشد است. بیش از 200 مشاور، تک تک کدرشته های دفترچه انتخاب رشته کارشناسی ارشد را تحلیل کرده اند و براساس اطلاعات کارنامه نهایی (کارنامه سبز) داوطلبان در سنوات گذشته آخرین رتبه قبولی را تعیین نموده اند تا شما همه شانس های قبولی خود را در تمام رشته محل های روزانه، نوبت دوم، پردیس خودگردان، پیام نور، غیرانتفاعی و مجازی مشاهده و بررسی نمایید. دقت نرم افزار براساس بررسی های قبولی سال گذشته بیش از 98 درصد ارزیابی شده است.</p>
                <p className="lineHeight">اما این پایان کار ما نبوده است. سخت ترین، مهم ترین و حساس ترین مرحله انتخاب رشته ارشد، اولویت بندی دقیق و صحیح کدرشته های دفترچه انتخاب رشته ارشد است. آنچه که در اولویت بندی اهمیت دارد، این است که براساس رتبه خود بهترین گرایش و معتبرترین دانشگاه را قبول شویم. آنچه که ما در اولویت بندی بی نظیر انتخاب رشته ارشد مشاوران تحصیلی به شما ارائه می کنیم، بهترین قبولی براساس رتبه شما را رقم خواهد زد.</p>
            </>
            }
        </div>
        <div className="container p-lg-5">
        {group===1?
            <>
                <h2 className="my-5 text-center w-100">ویژگی های نرم افزار انتخاب رشته ارشد وزارت</h2>
            </>
            :
            <>
                <h2 className="my-5 text-center w-100">ویژگی های نرم افزار انتخاب رشته ارشد</h2>
            </>
            }
            <div className={'d-flex flex-column flex-lg-row flex-lg-wrap'}>
                <div className="col-lg-4 col-12 mt-3">
                    <div className="box">
                        <img src={analyze} className="img-fluid" alt=""/>
                            <h3>آنالیز دقیق</h3>
                            <p>آنالیز دقیق شانس قبولی با دقتی بیش از 98 درصد براساس سهمیه و رتبه</p>
                    </div>
                </div>
                <div className="col-lg-4 col-12 mt-3">
                    <div className="box">
                        <img src={dice} className="img-fluid" alt=""/>
                        <h3>تعیین شانس قبولی</h3>
                        <p>تعیین شانس قبولی در 4 سطح قطعی، پراحتمال، خوشبینانه و کم احتمال</p>
                    </div>
                </div>

                <div className="col-lg-4 col-12 mt-3">
                    <div className="box">
                        <img src={dice2} className="img-fluid" alt=""/>
                        <h3>مشخص کردن احتمال قبولی</h3>
                        <p>مشخص کردن احتمال قبولی در تمامی کد رشته های دفترچه انتخاب رشته</p>
                    </div>
                </div>
                <div className="col-lg-4 col-12 mt-3">
                    <div className="box">
                        <img src={search} className="img-fluid" alt=""/>
                        <h3>جستجوی آسان</h3>
                        <p>جستجوی آسان براساس نوع دوره دانشگاهی، استان ها و رشته-گرایش</p>
                    </div>
                </div>
                <div className="col-lg-4 col-12 mt-3">
                    <div className="box">
                        <img src={priority} className="img-fluid" alt=""/>
                        <h3>اولویت بندی صحیح</h3>
                        <p>اولویت بندی صحیح بر مبنای اعتبار، کیفیت و رنگینگ دانشگاه ها</p>
                    </div>
                </div>
                <div className="col-lg-4 col-12 mt-3">
                    <div className="box">
                        <img src={priority2} className="img-fluid" alt=""/>
                        <h3>اولویت بندی دقیق</h3>
                        <p>اولویت بندی دقیق براساس کیفیت گرایش در بازار کار و امکان ادامه تحصیل</p>
                    </div>
                </div>
            </div>
            <div className="container p-lg-5">
            {group===1?
            <>
                <h2 className="my-5 text-center w-100">ویژگی های نرم افزار انتخاب رشته ارشد</h2>
                <p>قوانین زیر را با دقتی وسواس گونه در نرم افزار انتخاب رشته ارشد وزارت بهداشت رعایت کرده ایم تا دقیق ترین و بهترین انتخاب رشته ارشد را برای شما رقم بزنیم.</p>
            </>
            :
            <>
                <h2 className="my-5 text-center w-100">قوانین گزینش در انتخاب رشته ارشد 1403</h2>
                <p>قوانین زیر را با دقتی وسواس گونه در نرم افزار انتخاب رشته ارشد رعایت کرده ایم تا دقیق ترین و بهترین انتخاب رشته ارشد را برای شما رقم بزنیم.</p>
            </>
            }
            {group===1?
            <>
                <ul className={'d-flex flex-wrap list-unstyled'}>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'red'}}></i>رتبه در سهمیه، برای سهمیه آزاد رتبه ایست که برای گزینش نهایی در انتخاب رشته ارشد لحاظ می شود.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'green'}}></i>ظرفیت های جامانده از سهمیه های ایثارگران به سهمیه آزاد تعلق می گیرد.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'blue'}}></i>در بعضی کدرشته ها، به دلیل کم بودن ظرفیت پذیرش هیچ ظرفیتی به سهمیه ایثارگری 5 درصد تعلق نمی گیرد. </li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'yellow'}}></i>عدم کسب تراز حدنصاب توسط داوطلبان دارای سهمیه ایثارگری باعث می شود تا ظرفیت آنها به سهمیه آزاد برسد.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'orange'}}></i>سهیمه های ایثارگران هم در سهمیه خود مورد بررسی و گزینش قرار می گیرند و هم در سهمیه آزاد.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'purple'}}></i>مجاز بودن شما به انتخاب رشته ارشد دلیل قبولی قطعی و نهایی شما نیست. اولویت بندی دقیق و متناسب با رتبه شما، شانس قبولی شما را بسیار افزایش می دهد. </li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'brown'}}></i>در اولین اولویتی که پذیرفته شوید، قبولی نهایی شما ثبت می شود و قبولی در انتخاب های بعد از آن عملاً تشریفاتی و بی ارزش خواهد بود. پس درست اولویت بندی نمایید.</li>
                    {/* <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'pink'}}></i>قبولی در بعضی از کدرشته ها به مصاحبه های علمی و یا عقیدتی و سیاسی وابسته است. پس تنها رتبه شما در این رشته محل ها، ملاک گزینش نخواهد بود.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'grey'}}></i>امکان تغییر رشته در مقطع کارشناسی ارشد ممنوع است. در انتخاب رشته خود بی نقص و حساب شده و حرفه ای عمل کنید.</li> */}
                </ul>
            </>
            :
            <>
                <ul className={'d-flex flex-wrap list-unstyled'}>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'red'}}></i>ظرفیت های جامانده از سهمیه های ایثارگران به سهمیه آزاد تعلق می گیرد.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'green'}}></i>رتبه در سهمیه، برای سهمیه آزاد رتبه ایست که برای گزینش نهایی در انتخاب رشته ارشد لحاظ می شود.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'blue'}}></i>در بعضی کدرشته ها، به دلیل کم بودن ظرفیت پذیرش هیچ ظرفیتی به سهمیه ایثارگری 5 درصد تعلق نمی گیرد. </li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'yellow'}}></i>عدم کسب تراز حدنصاب توسط داوطلبان دارای سهمیه ایثارگری باعث می شود تا ظرفیت آنها به سهمیه آزاد برسد.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'orange'}}></i>سهیمه های ایثارگران هم در سهمیه خود مورد بررسی و گزینش قرار می گیرند و هم در سهمیه آزاد.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'purple'}}></i>مجاز بودن شما به انتخاب رشته ارشد دلیل قبولی قطعی و نهایی شما نیست. اولویت بندی دقیق و متناسب با رتبه شما، شانس قبولی شما را بسیار افزایش می دهد.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'brown'}}></i>در اولین اولویتی که پذیرفته شوید، قبولی نهایی شما ثبت می شود و قبولی در انتخاب های بعد از آن عملاً تشریفاتی و بی ارزش خواهد بود. پس درست اولویت بندی نمایید.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'pink'}}></i>قبولی در بعضی از کدرشته ها به مصاحبه های علمی و یا عقیدتی و سیاسی وابسته است. پس تنها رتبه شما در این رشته محل ها، ملاک گزینش نخواهد بود.</li>
                    <li><i class="bi bi-check-lg ml-2 lineHeight" style={{color: 'grey'}}></i>امکان تغییر رشته در مقطع کارشناسی ارشد ممنوع است. در انتخاب رشته خود بی نقص و حساب شده و حرفه ای عمل کنید.</li>
                </ul>
            </>
            }
                
            </div>
            <div className="container p-lg-5">
                {group===1?
                <>
                    <h2 className="my-5 text-center w-100">نکات مخفی و طلایی انتخاب رشته ارشد وزارت بهداشت 1403</h2>
                    <p>این نکات طلایی را در تعیین شانس های قبولی و اولویت بندی شما در نرم افزار رعایت کرده ایم تا در بهترین انتخابی که رتبه شما اجازه می دهد، پذیرفته شوید.</p>
                    <ul className={'d-flex flex-wrap list-unstyled'}>
                        <li><i class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>در بسیاری از کارنامه های سبز و یا همان کارنامه نهایی کارشناسی ارشد که توسط سازمان سنجش منتشر می شود، مشاهده می شود که برای سهمیه های ایثارگران کدهای عددی درج شده است که به آنها می گوید که در سهمیه ای که ثبت نام کرده اند مورد بررسی قرار نگرفته اند و در سهمیه آزاد مورد بررسی قرار گرفته اند. دلیل این امر یا عدم اعطای ظرفیت به آن سهمیه و یا تعداد بسیار پایین نفرات اختصاص داده شده برای آن سهمیه است. برای اینکه در حق قبولی شما در سهمیه تان به خاطر ظرفیت بسیار پایین آن  کد رشته ضایع نشود، در سهمیه آزاد هم مورد بررسی قرار گرفته اید.</li>
                        <li><i  class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>توجه داشته باشید که شانس قبولی شما فقط به رتبه شما بستگی ندارد. رتبه های بالاتر از شما در شانس قبولی شما بسیار بیشتر تأثیرگذار هستند. عدم انتخاب یک سری از کدرشته ها توسط رتبه های بهتر از شما باعث می شود تا شانس قبولی شما چندین برابر شود. در نتیجه در اولویت بندی که در نرم افزار انتخاب رشته ارشد طراحی کرده ایم، اعتبار و کیفیت گرایش و دانشگاه شرط بسیار اساسی بوده است.</li>
                        <li><i class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>در کارنامه های نهایی داوطلبان سهمیه های آزاد گاهی درج می شود که در سهمیه ای غیر از سهمیه آزاد قبول شده اند. در این صورت آنها از ظرفیت جامانده از سهمیه های ایثارگری قبول شده اند. براساس قانون ظرفیت های باقیمانده و پرنشده از سهمیه های دیگر که همگی به سهمیه آزاد می رسند. چنین نکته مهمی را نیز براساس داده های سنوات گذشته و تحلیل آنها، در تعیین شانس های قبولی شما در نظر گرفته ایم.</li>
                        <li><i class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>سهمیه جامانده و یا پرنشده از سهمیه 25 درصد ایثارگری ابتدا به سهمیه 5 درصد ایثارگری و سپس به سهمیه آزاد تعلق می گیرد. چنین نکته ای را نیز با تحلیل متخصصان براساس داده های سنوات گذشته و الگوی این تخصیص ظرفیت، برای انتخاب رشته ارشد سهمیه 5 درصد ایثارگری اعمال کرده ایم. </li>
                        {/* <li><i class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>سهمیه جامانده و یا پرنشده از سهمیه 25 درصد ایثارگری ابتدا به سهمیه 5 درصد ایثارگری و سپس به سهمیه آزاد تعلق می گیرد. چنین نکته ای را نیز با تحلیل متخصصان براساس داده های سنوات گذشته و الگوی این تخصیص ظرفیت، برای انتخاب رشته ارشد سهمیه 5 درصد ایثارگری اعمال کرده ایم.</li> */}

                    </ul>
                </>
                :
                <>
                    <h2 className="my-5 text-center w-100">نکات مخفی و طلایی انتخاب رشته ارشد 1403</h2>
                    <p>این نکات طلایی را در تعیین شانس های قبولی و اولویت بندی شما در نرم افزار رعایت کرده ایم تا در بهترین انتخابی که رتبه شما اجازه می دهد، پذیرفته شوید.</p>
                    <ul className={'d-flex flex-wrap list-unstyled'}>
                        <li><i class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>براساس قوانین جدید با قبولی در دوره های روزانه و سپس انصراف از آنها از کنکور سال بعد محروم نخواهید شد. پس به بهانه محرومیت، کدرشته های نوبت دوم و یا شامل شهریه را بالاتر از انتخاب های روزانه قرار ندهید.</li>
                        <li><i  class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>در بسیاری از کارنامه های سبز و یا همان کارنامه نهایی کارشناسی ارشد که توسط سازمان سنجش منتشر می شود، مشاهده می شود که برای سهمیه های ایثارگران کدهای عددی درج شده است که به آنها می گوید که در سهمیه ای که ثبت نام کرده اند مورد بررسی قرار نگرفته اند و در سهمیه آزاد مورد بررسی قرار گرفته اند. دلیل این امر یا عدم اعطای ظرفیت به آن سهمیه و یا تعداد بسیار پایین نفرات اختصاص داده شده برای آن سهمیه است. برای اینکه در حق قبولی شما در سهمیه تان به خاطر ظرفیت بسیار پایین آن  کد رشته ضایع نشود، در سهمیه آزاد هم مورد بررسی قرار گرفته اید.</li>
                        <li><i class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>توجه داشته باشید که شانس قبولی شما فقط به رتبه شما بستگی ندارد. رتبه های بالاتر از شما در شانس قبولی شما بسیار بیشتر تأثیرگذار هستند. عدم انتخاب یک سری از کدرشته ها توسط رتبه های بهتر از شما باعث می شود تا شانس قبولی شما چندین برابر شود. در نتیجه در اولویت بندی که در نرم افزار انتخاب رشته ارشد طراحی کرده ایم، اعتبار و کیفیت گرایش و دانشگاه شرط بسیار اساسی بوده است.</li>
                        <li><i class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>در کارنامه های نهایی داوطلبان سهمیه های آزاد گاهی درج می شود که در سهمیه ای غیر از سهمیه آزاد قبول شده اند. در این صورت آنها از ظرفیت جامانده از سهمیه های ایثارگری قبول شده اند. براساس قانون ظرفیت های باقیمانده و پرنشده از سهمیه های دیگر که همگی به سهمیه آزاد می رسند. چنین نکته مهمی را نیز براساس داده های سنوات گذشته و تحلیل آنها، در تعیین شانس های قبولی شما در نظر گرفته ایم.</li>
                        <li><i class="bi bi-star-fill ml-2 lineHeight" style={{color: 'gold'}}></i>سهمیه جامانده و یا پرنشده از سهمیه 25 درصد ایثارگری ابتدا به سهمیه 5 درصد ایثارگری و سپس به سهمیه آزاد تعلق می گیرد. چنین نکته ای را نیز با تحلیل متخصصان براساس داده های سنوات گذشته و الگوی این تخصیص ظرفیت، برای انتخاب رشته ارشد سهمیه 5 درصد ایثارگری اعمال کرده ایم.</li>

                    </ul>
                </>
                }
            </div>
            {/*<div className={'d-flex justify-content-center mt-5'}>*/}
            {/*    <div className="col-lg-6 col-12">*/}
            {/*        <div className="box">*/}
            {/*            <video className={'w-100'} width="400" controls>*/}
            {/*                <source src="mov_bbb.mp4" type="video/mp4"/>*/}
            {/*            </video>*/}
            {/*            <h3>ویدیوی استفاده از نرم‌افزار</h3>*/}
            {/*        </div>*/}
            {/*    </div>*/}
            {/*</div>*/}
            {/* <div className="col-12 mt-5">
                <div className="box p-0 p-lg-5">
                   <ul className={'d-flex flex-wrap'}>
                       <li className={'col-6 col-lg-4 mt-2'}>
                           <a href="">انتخاب رشته ارشد کامپیوتر</a>
                       </li>
                       <li className={'col-6 col-lg-4 mt-2'}>
                           <a href="">انتخاب رشته ارشد کامپیوتر</a>
                       </li>
                       <li className={'col-6 col-lg-4 mt-2'}>
                           <a href="">انتخاب رشته ارشد کامپیوتر</a>
                       </li>
                       <li className={'col-6 col-lg-4 mt-2'}>
                           <a href="">انتخاب رشته ارشد کامپیوتر</a>
                       </li>
                       <li className={'col-6 col-lg-4 mt-2'}>
                           <a href="">انتخاب رشته ارشد کامپیوتر</a>
                       </li>
                   </ul>
                </div>
            </div> */}
            <div className={'d-flex flex-lg-row flex-column'}>
                {group === 1 ? 
                    <div className={'col-12 col-lg-8'}>
                        <ul className={'d-flex flex-wrap list-unstyled text-center'}>
                            {/* <li className={'col-6 col-lg-4 mt-5'}>
                                <a href="https://moshaveranetahsili.ir/%d8%b1%d8%a7%d9%87%d9%86%d9%85%d8%a7%db%8c-%d9%86%d8%b1%d9%85-%d8%a7%d9%81%d8%b2%d8%a7%d8%b1-%d8%a7%d9%86%d8%aa%d8%ae%d8%a7%d8%a8-%d8%b1%d8%b4%d8%aa%d9%87-%d9%85%d8%b4%d8%a7%d9%88%d8%b1%d8%a7%d9%86/" target="_blank" rel="noopener">راهنمای نرم افزار انتخاب رشته</a>
                            </li> */}
                            <li className={'col-6 col-lg-4 mt-5'}>
                                <a href="https://moshaveranetahsili.ir/product/%d9%85%d8%b4%d8%a7%d9%88%d8%b1%d9%87-%d8%aa%d9%84%d9%81%d9%86%db%8c-%d8%a7%d9%86%d8%aa%d8%ae%d8%a7%d8%a8-%d8%b1%d8%b4%d8%aa%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-1403/" target="_blank" rel="noopener">مشاوره تلفنی</a>
                            </li>
                            <li className={'col-6 col-lg-4 mt-5'}>
                                <a href="https://moshaveranetahsili.ir/%d8%af%d9%81%d8%aa%d8%b1%da%86%d9%87-%d8%a7%d9%86%d8%aa%d8%ae%d8%a7%d8%a8-%d8%b1%d8%b4%d8%aa%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%db%b1%db%b4%db%b0%db%b2-%db%b1%db%b4%db%b0%db%b1/" target="_blank" rel="noopener">دانلود دفترچه انتخاب رشته 1403 </a>
                            </li>
                            <li className={'col-6 col-lg-4 mt-5'}>
                                <a href="https://moshaveranetahsili.ir/%d8%aa%d9%85%d8%a7%d8%b3-%d8%a8%d8%a7-%d9%85%d8%a7/" target="_blank" rel="noopener">تماس با ما</a>
                            </li>
                        </ul>
                    </div> 
                    : ''
                }
                {/* <div className={'col-12 col-lg-4'}> */}
                    <div className={'w-100 d-flex justify-content-center mt-4'}>
                        <SamandehiLogo
                            // optional true | false
                            sid="249476"
                            sp="uiwkaodspfvlaodsjyoegvka"
                        />
                        <EnamadLogo/>
                    </div>
                {/* </div> */}
            </div>
            <div className={'text-center input-box bg-secondary text-white mt-5 p-2'}>
                <p className={'m-0'}>تمامی حقوق نرم‌افزار انتخاب رشته ارشد ۱۴۰۱ متعلق به وب سایت مشاوران تحصیلی است</p>
            </div>
        </div>
    </div>
}

import React, {useState} from 'react'
import hero from '../assets/aks.jpeg'
import Prediction from "../Components/Prediction";
import SpinnerLoading from "../Components/Spinner";
import SamandehiLogo from "../Components/Samandehi/SamandehiLogo";
import EnamadLogo from "../Components/Samandehi/EnamadLogo";
import Table from 'react-bootstrap/Table'
import { useHistory } from "react-router-dom";
// Demo styles, see 'Styles' section below for some notes on use.
import 'react-accessible-accordion/dist/fancy-example.css';
import Collapsible from 'react-collapsible';

export default function Home({group,year}) {
    const [loading,setLoading] = useState(false)
    const history = useHistory();
    function handleClick() {
        history.push("/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت");
      }
    return <div className={'bg-main'}>
        <SpinnerLoading
            show={loading}/>
        <div className={' pb-4 d-flex flex-column container align-items-center'}>
            <div className={'d-flex justify-content-start w-100'}>
                <img className="d-block my-4" src='./logo.png' alt=""/>
            </div>
            <div className={'input-box d-flex w-100 mb-5 p-5 flex-column text-center bg-primary text-white'}>
                <h2>تخمین رتبه فقط
                    یک آگاهی حدودی از رتبه نیست</h2>
                <h4 className={'mt-5'}>تخمین رتبه سرآغاز تحلیل درست شرایط و نظم بخشیدن به افکار مزاحم، پراکنده، غلط و آشفته است</h4>
            </div>
            {/* { group === 1 ?
                <div className={'input-box d-flex w-100 mb-5 p-5 flex-column text-center'}>
                    <h4 className={'mt-5'}>برای انتخاب رشته ارشد وزارت بهداشت از نرم افزار انتخاب رشته مرکز مشاوران تحصیلی استفاده کنید</h4>
                    <a target="_blank" href="https://apps.moshaveranetahsili.ir/%D9%86%D8%B1%D9%85-%D8%A7%D9%81%D8%B2%D8%A7%D8%B1-%D8%A7%D9%86%D8%AA%D8%AE%D8%A7%D8%A8-%D8%B1%D8%B4%D8%AA%D9%87-%D8%A8%D9%87%D8%AF%D8%A7%D8%B4%D8%AA-1400">نرم افزار انتخاب رشته ارشد وزارت بهداشت</a>
                </div>
            :
                <div className={'input-box d-flex w-100 mb-5 p-5 flex-column text-center'}>
                    <h4 className={'mt-5'}>برای انتخاب رشته ارشد وزارت علوم از نرم افزار انتخاب رشته مرکز مشاوران تحصیلی استفاده کنید</h4>
                    <a target="_blank" href="https://apps.moshaveranetahsili.ir/%D9%86%D8%B1%D9%85-%D8%A7%D9%81%D8%B2%D8%A7%D8%B1-%D8%A7%D9%86%D8%AA%D8%AE%D8%A7%D8%A8-%D8%B1%D8%B4%D8%AA%D9%87-%D8%B9%D9%84%D9%88%D9%85-1400">نرم افزار انتخاب رشته ارشد وزارت علوم</a>
                </div>
            } */}
            <div className={'input-box d-flex flex-wrap align-item-center justify-content-center py-5  mb-5 w-100'} >
                <div className="d-flex flex-column col-12 col-lg-6 align-self-center mt-1">
                    <div className="mb-5 text-align-center">
                        {group === 1 ?
                            <a href="/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت" target="_blank">نرم افزار انتخاب رشته ارشد وزارت بهداشت</a>
                            :
                            <a href="/نرم-افزار-انتخاب-رشته-ارشد" target="_blank">نرم افزار انتخاب رشته ارشد وزارت علوم</a>
                        }
                    </div>
                    <Prediction setLoading={setLoading} group={group}/>
                </div>
                <div className="col-lg-6 col-12 d-none d-lg-flex flex-column align-items-center">
                
                    <img src={hero} className="hero-img align-self-center"/>
                </div>
            </div>
            <div className="row w-100 justify-content-center">
                <div className="pt-4 pt-lg-0  d-flex flex-column">
                        <div className={'input-box p-5 mb-5'}>
                            <h2 className="display-5 fw-bold mb-5  text-center">تخمین رتبه کارشناسی ارشد</h2>
                            <div className=" mx-auto">
                                <p>
تخمین رتبه کارشناسی ارشد، به منظور اطلاع از رتبه تقریبی براساس درصدهایی که داوطلبان می زنند صورت می گیرد. در تخمین رتبه کارشناسی ارشد باید موراد زیر را در نظر گرفت تا بتوان تخمین رتبه دقیق و صحیحی ارائه داد:</p>
                              <ol >
                                   <li className={' mt-3 p-0'}>محاسبه دقیق درصدهای هر درس:  حاصلضرب تعداد سوالات صحیح زده شده در عدد 3 را از تعداد سوالات غلط زده شده کم می کنیم و سپس بر حاصلضرب تعداد کل سوالات در عدد 3، تقسیم می کنیم. توجه کنید که هر سوال صحیح سه برابر یک سوال غلط ارزش دارد و یا به عبارت دیگر هر سه سوال غلط یک جواب صحیح را از بین می برد.</li>
                                  <li className={' mt-3 p-0'}>معدل: تأثیر معدل در رتبه کنکور کارشناسی ارشد به اندازه بیست درصد است و نمره تراز کل با در نظر گرفتن معدل شما به دست می آید.</li>
                                 <li className={' mt-3 p-0'}>معدل موثر: سازمان سنجش برای آزمون ارشد، معدل داوطلب را با توجه به رشته و دانشگاهی که دارد در یک ضریب افزایشی ضرب می کند. کمترین مقدار این ضریب 1 و بیشترین مقدار این ضریب حدود 1/1 می باشد. هر قدر در دانشگاه معتبرتری، مدرک کارشناسی خود را گرفته باشید، تأثیر معدل شما در نمره تراز شما بیشتر است.</li>
                                 <li className={' mt-3 p-0'}>در نظر گرفتن سهمیه: سهمیه های آزاد و ایثارگری تفاوت های بسیار زیادی را در تخمین رتبه ارشد به وجود می آورند. در تخمین رتبه ارشد در سهمیه آزاد، رتبه کشوری یا بدون اعمال سهمیه، اهمیت چندانی ندارد ولی در سهمیه 5 درصد ایثارگران، رتبه کشوری برای اطلاع از اختلاف واقعی رتبه با رتبه های سهمیه آزاد اهمیت دارد.</li>
                              </ol>
                            </div>
                        </div>
                </div>
            </div>
            <div className={'input-box d-flex w-100 mb-5 p-5 flex-column text-center'}>
                    <h2 className="display-5 fw-bold mb-5  text-center">نرم افزار تخمین رتبه ارشد 1403</h2>
                    <p>نرم افزار تخمین رتبه ارشد 1403، براساس اطلاعات رتبه ها و درصدهای رتبه های سال های گذشته محاسبه می شود. برای محاسبه دقیق تر رتبه تخمینی ارشد 1403 از طریق نرم افزار، احتساب سطح سوالات نسبت به سنوات گذشته اهمیت بسیار زیادی دارد.
در نرم افزار تخمین رتبه ارشد 1403 مرکز مشاوران تحصیلی با در نظر گرفتن پارامترهایی همچون تفاوت سطح سوالات و تغییر در تعداد داوطلبان سعی می شود تا تخمین رتبه دقیق تری به داوطلبان کنکور ارشد ارائه شود.</p>
                </div>
                <div className={'input-box d-flex w-100 mb-5 p-5 flex-column text-center'}>
                    <h2 className="display-5 fw-bold mb-5  text-center">تخمین رتبه ارشد سهمیه 5 درصد ایثارگری</h2>
                    <p>تخمین رتبه ارشد سهمیه 5 درصد ایثارگری نیز در نرم افزار مرکز مشاوران تحصیلی گنجانده شده است. نرم افزار تخمین رتبه ارشد، تنها نرم افزاریست که با دقت بسیار بالایی تخمین رتبه سهمیه 5 درصد ایثارگری کنکور ارشد را انجام می هد. علاوه بر رتبه در سهمیه ایثارگری، رتبه کشوری داوطبان دارای سهمیه 5 درصد ایثارگری نیز به آنها نمایش داده می شود.</p>             
                </div>
                <div className={'input-box d-flex w-100 mb-5 p-5 flex-column text-center'}>
                    <h2 className="display-5 fw-bold mb-5  text-center">رشته های کنکور ارشد تحت پوشش نرم افزار تخمین رتبه ارشد 1403 </h2>
                    <p>رشته های زیر در سامانه و نرم افزار تخمین رتبه ارشد 1403 مرکز مشاوران تحصیلی تحت پوشش قرار گرفته اند:</p>      
                    <h3 className={'mt-5'}>تخمین رتبه کنکور ارشد گروه علوم انسانی </h3>
                    <p>رشته های زیر از گروه انسانی تحت پوشش نرم افزار تخمین رتبه کنکور ارشد هستند</p>
                    <p style={{fontSize:12}}>جدول رشته های تحت پوشش نرم افزار تخمین رتبه ارشد گروه علوم انسانی</p>      
                    <Table striped bordered hover responsive>
                            <tr>
                                <td>حقوق</td>
                                <td>مدیریت</td>
                                <td>حسابداری</td>
                            </tr>
                        <tbody>
                            <tr>
                                <td>روانشناسی</td>
                                <td>علوم تربیتی</td>
                                <td>زبان انگلیسی</td>
                            </tr>
                            <tr>
                                <td>مشاوره</td>
                                <td>مدیریت کسب و کار (MBA)</td>
                                <td>اقتصاد</td>
                            </tr>
                            <tr>
                                <td>علوم اجتماعی</td>
                                <td>علوم سیاسی</td>
                                <td>تربیت بدنی و علوم ورزشی</td>
                            </tr>
                            <tr>
                                <td> تاریخ</td>
                                <td>زبان و ادبیات فارسی</td>
                                <td>علوم قرآن و حدیث</td>
                            </tr>
                            <tr>
                                <td>فقه و حقوق</td>
                                <td>فلسفه</td>
                                <td>مدیریت جهانگردی</td>
                            </tr>
                            <tr>
                                <td>جغرافیا</td>
                                <td>علوم ارتباطات اجتماعی</td>
                                <td>مددکاری اجتماعی</td>
                            </tr>
                            <tr>
                                <td>باستان شناسی</td>
                                <td>مطالعات زنان</td>
                                <td>زبانشناسی</td>
                            </tr>
                            <tr>
                                <td>زبانهای باستانی ایران</td>
                                <td>سنجش از دور و سیستم اطلاعات جغرافیایی (GIS)</td>
                                <td>ادیان و عرفان و تاریخ فرهنگ و تمدن اسلامی</td>
                            </tr>
                        </tbody>
                    </Table>

                    <h3 className={'mt-5'}>تخمین رتبه کنکور ارشد گروه مهندسی </h3>
                    <p>رشته های زیراز گروه مهندسی تحت پوشش نرم افزار تخمین رتبه کنکور ارشد هستند</p>
                    <p style={{fontSize:12}}>جدول رشته های تحت پوشش نرم افزار تخمین رتبه ارشد گروه مهندسی </p>      
                    <Table striped bordered hover responsive>
                            <tr>
                                <td>برق</td>
                                <td>عمران</td>
                                <td>کامپیوتر</td>
                                <td>مکانیک</td>
                            </tr>
                        <tbody>
                            <tr>
                                <td>صنایع</td>
                                <td>مهندسی شیمی</td>
                                <td>فناوری اطلاعات (آی تی-IT)</td>
                                <td>هوافضا</td>
                            </tr>
                            <tr>
                                <td>مواد و متالورژی</td>
                                <td>نقشه برداری</td>
                                <td>پلیمر</td>
                                <td>نفت</td>
                            </tr>
                            <tr>
                                <td>مهندسی معماری کشتی</td>
                                <td>مهندسی ابزار دقیق و اتوماسیون صنایع نفت</td>
                                <td>مهندسی شیمی-بیوتکنولوژی و داروسازی</td>
                                <td>مهندسی معدن</td>
                            </tr>
                        </tbody>
                    </Table>

                    <h3 className={'mt-5'}>تخمین رتبه کنکور ارشد گروه علوم پایه </h3>
                    <p>رشته های زیراز گروه علوم پایه تحت پوشش نرم افزار تخمین رتبه کنکور ارشد هستند</p>
                    <p style={{fontSize:12}}>جدول رشته های تحت پوشش نرم افزار تخمین رتبه ارشد گروه علوم پایه</p>      
                    <Table striped bordered hover responsive>
                            <tr>
                                <td>شیمی</td>
                                <td>زیست شناسی سلولی و مولکولی</td>
                                <td>فیزیک</td>
                            </tr>
                        <tbody>
                            <tr>
                                <td>ریاضی</td>
                                <td>علوم کامپیوتر</td>
                                <td>آمار</td>
                            </tr>
                            <tr>
                                <td>زمین شناسی</td>
                                <td>علوم شناختی</td>
                                <td>فوتونیک</td>
                            </tr>
                            <tr>
                                <td>تاریخ و فلسفه علم</td>
                                <td colspan="2">ژئوفیزیک و هواشناسی</td>                               
                            </tr>
                        </tbody>
                    </Table>

                    <h3 className={'mt-5'}>تخمین رتبه کنکور ارشد گروه هنر</h3>
                    <p>رشته های زیراز گروه هنر تحت پوشش نرم افزار تخمین رتبه کنکور ارشد هستند</p>
                    <p style={{fontSize:12}}>جدول رشته های تحت پوشش نرم افزار تخمین رتبه ارشد گروه هنر</p>      
                    <Table striped bordered hover responsive>
                            <tr>
                                <td>معماری</td>
                                <td>موسیقی</td>
                                <td>هنرهای نمایشی و سینما</td>
                            </tr>
                        <tbody>
                            <tr>
                                <td>هنرهای ساخت و معماری</td>
                                <td colspan="2">برنامه ریزی شهری، منطقه ای و مدیریت شهری</td>                               
                            </tr>
                        </tbody>
                    </Table>

                    <h3 className={'mt-5'}>تخمین رتبه کنکور ارشد گروه کشاورزی</h3>
                    <p>رشته های زیراز گروه کشاورزی تحت پوشش نرم افزار تخمین رتبه کنکور ارشد هستند</p>
                    <p style={{fontSize:12}}>جدول رشته های تحت پوشش نرم افزار تخمین رتبه ارشد گروه کشاورزی</p>      
                    <Table striped bordered hover responsive>
                    <tbody>
                            <tr>
                                <td>زراعت و اصلاح نباتات</td>
                                <td>علوم دامی (دام و طیور)</td>
                                <td>مهندسی صنایع غذایی</td>
                            </tr>
                            <tr>
                                <td>باغبانی</td>
                                <td>شیلات</td>
                                <td>بیوتکنولوژی کشاورزی</td>
                            </tr>
                            <tr>
                                <td>مرتع و آبخیزداری</td>
                                <td>توسعه روستایی</td>
                                <td>مهندسی مکانیک بیوسیستم</td>
                            </tr>
                        </tbody>
                    </Table>

                    <h3 className={'mt-5'}>تخمین رتبه کنکور ارشد گروه دامپزشکی</h3>
                    <p></p>
                    <p style={{fontSize:12}}>جدول رشته های تحت پوشش نرم افزار تخمین رتبه ارشد گروه دامپزشکی</p>      
                    <Table striped bordered hover responsive>
                    <tbody>
                            <tr>
                                <td>باکتری شناسی دامپزشکی</td>
                                <td>بهداشت و کنترل کیفی مواد غذایی</td>
                            </tr>
                        </tbody>
                    </Table>

                    {/* <Accordion>
                        <AccordionItem>
                            <AccordionItemHeading >
                                <AccordionItemButton style={{float: 'right'}}>
                                استفاده از نرم افزار تخمین رتبه ارشد به صورت رایگان است؟
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                                <p style={{marginRight:20}}>
                                در نرم افزار، امکان یکبار تخمین رتبه ارشد رایگان برای شما وجود دارد. برای تخمین رتبه های بعدی از پکیج های تعریف شده می توانید استفاده کنید.
                                </p>
                            </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem>
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                    Is free will real or just an illusion?
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                                <p>
                                    In ad velit in ex nostrud dolore cupidatat consectetur
                                    ea in ut nostrud velit in irure cillum tempor laboris
                                    sed adipisicing eu esse duis nulla non.
                                </p>
                            </AccordionItemPanel>
                        </AccordionItem>
                    </Accordion> */}
                    <h3 className={'mt-5'}> پرسش و پاسخ</h3>
                    <Collapsible open triggerStyle={{color:'red', backgroundColor: 'lightblue', fontSize: 'bold'}} trigger="استفاده از نرم افزار تخمین رتبه ارشد به صورت رایگان است؟">
                        <p>در نرم افزار، امکان یکبار تخمین رتبه ارشد رایگان برای شما وجود دارد. برای تخمین رتبه های بعدی از پکیج های تعریف شده می توانید استفاده کنید.
                        </p>
                    </Collapsible>
                    <Collapsible triggerStyle={{color:'red', backgroundColor: 'lightblue'}} trigger="تفاوت نرم افزار تخمین رتبه ارشد مرکز مشاوران تحصیلی با سایر نرم افزارهای تخمین رتبه در چیست؟">
                        <p>دقت بسیار بالا و تحلیل تعداد بسیار زیادی از داده های رتبه ها و درصدهای داوططلبان کنکور ارشد از تفاوت های اساسی نرم افزار تخمین رتبه ارشد مشاوران تحصیلی با سایر نرم افزارهاست. همچنین بازه ارائه شده برای رتبه تخمین زده شده با توجه به تفاوت در سختی یا آسانی کنکور نمایش داده می شود.
                        </p>
                    </Collapsible>
                    <Collapsible triggerStyle={{color:'red', backgroundColor: 'lightblue'}} trigger="رتبه تخمین زده شده تا چه حد می تواند به رتبه واقعی کنکور ارشد نزدیک باشد؟">
                        <p>بازه ای که برای رتبه تخمینی نمایش داده می شود به گونه ای تعریف شده است که سطح سختی و آسانی سوالات در نظر گرفته شده باشد. حد پایینی برای کنکرو سخت تر و حد بالایی برای کنکور آسان تر و حد وسط برای کنکور متوسط طراحی شده است.
                        </p>
                    </Collapsible>
                </div>
            {/* {group === 1 ?
                <div className="row w-100 justify-content-center">
                    <div className="pt-4 pt-lg-0  d-flex flex-column">
                            <div className={'input-box p-5 mb-5'}>

                                <h2 className="display-5 fw-bold mb-5  text-center">نرم‌افزار تخمین رتبه ارشد وزارت بهداشت</h2>
                                <div className=" mx-auto">
                                <ul className={'d-flex flex-wrap'}>
                                    <li className={'col-12 col-lg-6 mt-3 p-0'}>در وزارت بهداشت، سقف تراز ۱۰۰ نمره است</li>
                                    <li className={'col-12 col-lg-6 mt-3 p-0'}>درصدهای شما به صورت درس به درس، با بالاترین درصد زده شده در هر درس ترازدهی می‌شود</li>
                                    <li className={'col-12 col-lg-6 mt-3 p-0'}>سقف تاثیر معدل، ۲۰ نمره است</li>
                                    <li className={'col-12 col-lg-6 mt-3 p-0'}>تعداد کم داوطلبان سهمیه‌های ایثارگران، اهمیت رتبه کشوری آنها را بیشتر می‌کند</li>
                                </ul>
                                </div>
                            </div>
                    </div>
                </div>
            :
                <div className="row w-100 justify-content-center">
                    <div className="pt-4 pt-lg-0  d-flex flex-column">
                            <div className={'input-box p-5 mb-5'}>
                                <h2 className="display-5 fw-bold mb-5  text-center">نرم‌افزار تخمین رتبه ارشد وزارت علوم</h2>
                                <div className=" mx-auto">
                                <ul className={'d-flex flex-wrap'}>
                                    <li className={'col-12 col-lg-6 mt-3 p-0'}>در وزارت علوم، سقف تراز ۱۰۰۰۰ نمره است</li>
                                    <li className={'col-12 col-lg-6 mt-3 p-0'}>میانگین موزون درصدهای شما براساس بالاترین میانگین موزون در رشته شما، ترازدهی می‌شود</li>
                                    <li className={'col-12 col-lg-6 mt-3 p-0'}>سقف تاثیر معدل، ۲۰۰۰ نمره است</li>
                                    <li className={'col-12 col-lg-6 mt-3 p-0'}>رتبه‌های سهمیه ایثارگران به تعداد داوطلبان آنها بستگی دارد</li>
                                </ul>
                                </div>
                            </div>
                    </div>
                </div>
            } */}

        {/* {group === 1 ?
            <div className="row w-100 justify-content-center">
                <div className="pt-4 pt-lg-0  d-flex flex-column">
                        <div className={'input-box p-5 mb-5'}>
                            <h2 className="display-5 fw-bold mb-5  text-center">لیست رشته‌های وزارت بهداشت</h2>
                            <div className=" mx-auto">
                              <ul className={'d-flex flex-wrap'}>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a2%d9%85%d8%a7%d8%b1-%d8%b2%db%8c%d8%b3%d8%aa%db%8c-1400/">تخمین رتبه ارشد آمار زیستی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%d9%be%db%8c%d8%af%d9%85%db%8c%d9%88%d9%84%d9%88%da%98%db%8c-1400/">تخمین رتبه ارشد اپیدمیولوژی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%d8%b1%d8%b2%db%8c%d8%a7%d8%a8%db%8c-%d9%81%d9%86%d8%a7%d9%88%d8%b1%db%8c-%d8%b3%d9%84%d8%a7%d9%85%d8%aa-1400/">تخمین رتبه ارشد ارزیابی فناوری سلامت (HTA)</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%d8%b1%da%af%d9%88%d9%86%d9%88%d9%85%db%8c-1400/">تخمین رتبه ارشد ارگونومی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%d8%b9%d8%b6%d8%a7%db%8c-%d9%85%d8%b5%d9%86%d9%88%d8%b9%db%8c-1400/">تخمین رتبه ارشد اعضای مصنوعی و وسایل کمکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%d9%82%d8%aa%d8%b5%d8%a7%d8%af-%d8%a8%d9%87%d8%af%d8%a7%d8%b4%d8%aa-1400/">تخمین رتبه ارشد اقتصاد بهداشت</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%da%a9%d9%88%d9%84%d9%88%da%98%db%8c-%d8%a7%d9%86%d8%b3%d8%a7%d9%86%db%8c-1400/">تخمین رتبه ارشد اکولوژی انسانی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%d9%86%d9%81%d9%88%d8%b1%d9%85%d8%a7%d8%aa%db%8c%da%a9-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد انفورماتیک پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%d9%86%da%af%d9%84-%d8%b4%d9%86%d8%a7%d8%b3%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد انگل شناسی پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%db%8c%d9%85%d9%86%db%8c-%d8%b4%d9%86%d8%a7%d8%b3%db%8c-1400/">تخمین رتبه ارشد ایمنی شناسی پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a2%d9%85%d9%88%d8%b2%d8%b4-%d8%a8%d9%87%d8%af%d8%a7%d8%b4%d8%aa-1400/">تخمین رتبه ارشد آموزش بهداشت و ارتقاء سلامت</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a2%d9%85%d9%88%d8%b2%d8%b4-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد آموزش پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a2%d9%85%d9%88%d8%b2%d8%b4-%d9%87%d9%88%d8%b4%d8%a8%d8%b1%db%8c-1400/">تخمین رتبه ارشد آموزش هوشبری</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a8%d9%87%d8%af%d8%a7%d8%b4%d8%aa-%d9%85%d8%ad%db%8c%d8%b7-1400/">تخمین رتبه ارشد بهداشت محیط</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a8%d9%87%d8%af%d8%a7%d8%b4%d8%aa-%d9%85%d9%88%d8%a7%d8%af-%d8%ba%d8%b0%d8%a7%db%8c%db%8c-1400/">تخمین رتبه ارشد بهداشت و ایمنی مواد غذایی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a8%db%8c%d9%86%d8%a7%db%8c%db%8c-%d8%b3%d9%86%d8%ac%db%8c-1400/">تخمین رتبه ارشد بینایی سنجی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a8%db%8c%d9%88%d8%a7%d9%84%da%a9%d8%aa%d8%b1%db%8c%da%a9-1400/">تخمین رتبه ارشد بیوالکتریک</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a8%db%8c%d9%88%d8%aa%da%a9%d9%86%d9%88%d9%84%d9%88%da%98%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد بیوتکنولوژی پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a8%db%8c%d9%88%d8%b4%db%8c%d9%85%db%8c-%d8%a8%d8%a7%d9%84%db%8c%d9%86%db%8c-1400/">تخمین رتبه ارشد بیوشیمی بالینی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%da%a9%d9%86%d8%aa%d8%b1%d9%84-%d9%86%d8%a7%d9%82%d9%84%db%8c%d9%86-%d8%a8%db%8c%d9%85%d8%a7%d8%b1%db%8c%d9%87%d8%a7-14/">تخمین رتبه ارشد بیولوژی و کنترل ناقلین بیماریها</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%85%d9%87%d9%86%d8%af%d8%b3%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-%d8%b2%db%8c%d8%b3%d8%aa-%d9%85%d9%88%d8%a7%d8%af14/">تخمین رتبه ارشد بیومواد</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%be%d8%af%d8%a7%d9%81%d9%86%d8%af-%d8%ba%db%8c%d8%b1-%d8%b9%d8%a7%d9%85%d9%84-1400/">تخمین رتبه ارشد پدافند غیرعامل در نظام سلامت</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%be%d8%b1%d8%b3%d8%aa%d8%a7%d8%b1%db%8c-1400/">تخمین رتبه ارشد پرستاری</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%aa%d8%a7%d8%b1%db%8c%d8%ae-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد تاریخ علوم پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%b7%d8%a8%db%8c%d8%b9%db%8c-%d8%af%d8%a7%d8%b1%d9%88%db%8c%db%8c-%d8%af%d8%b1%db%8c%d8%a7%db%8c%db%8c-1400/">تخمین رتبه ارشد ترکیبات طبیعی و دارویی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%aa%d8%ba%d8%b0%db%8c%d9%87-1400/">تخمین رتبه ارشد تغذیه</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%a7%d8%aa%d8%a7%d9%82-%d8%b9%d9%85%d9%84-1400/">تخمین رتبه ارشد تکنولوژی اتاق عمل</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%aa%da%a9%d9%86%d9%88%d9%84%d9%88%da%98%db%8c-%da%af%d8%b1%d8%af%d8%b4-%d8%ae%d9%88%d9%86-1400/">تخمین رتبه ارشد تکنولوژی گردش خون (پرفیوژن)</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%b1%d9%88%d8%a7%d9%86%d8%b4%d9%86%d8%a7%d8%b3%db%8c-%d8%a8%d8%a7%d9%84%db%8c%d9%86%db%8c-%d8%a8%d9%87%d8%af%d8%a7%d8%b4%d8%aa-1400/">تخمین رتبه ارشد روانشناسی بالینی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%da%98%d9%86%d8%aa%db%8c%da%a9-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد ژنتیک انسانی (پزشکی)</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%da%98%d9%88%d8%b1%d9%86%d8%a7%d9%84%db%8c%d8%b3%d9%85-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد ژورنالیسم پزشکی (سلامت و رسانه)</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%b3%d9%84%d8%a7%d9%85%d8%aa-%d8%b3%d8%a7%d9%84%d9%85%d9%86%d8%af%db%8c-1400/">تخمین رتبه ارشد سلامت سالمندی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%b3%d9%84%d8%a7%d9%85%d8%aa-%d9%88-%d8%aa%d8%b1%d8%a7%d9%81%db%8c%da%a9-1400/">تخمین رتبه ارشد سلامت و ترافیک</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%b3%d9%84%d8%a7%d9%85%d8%aa-%d9%88-%d8%b1%d9%81%d8%a7%d9%87-%d8%a7%d8%ac%d8%aa%d9%85%d8%a7%d8%b9%db%8c-1400/">تخمین رتبه ارشد سلامت و رفاه اجتماعی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%b3%d9%85-%d8%b4%d9%86%d8%a7%d8%b3%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد سم شناسی پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%b4%d9%86%d9%88%d8%a7%db%8c%db%8c-%d8%b4%d9%86%d8%a7%d8%b3%db%8c-1400/">تخمین رتبه ارشد شنوایی شناسی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%b4%db%8c%d9%85%db%8c-%d8%af%d8%a7%d8%b1%d9%88%db%8c%db%8c-%d8%a8%d9%87%d8%af%d8%a7%d8%b4%d8%aa-1400/">تخمین رتبه ارشد شیمی دارویی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%b9%d9%84%d9%88%d9%85-%d8%aa%d8%b4%d8%b1%db%8c%d8%ad%db%8c-1400/">تخمین رتبه ارشد علوم تشریحی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%b5%d9%86%d8%a7%db%8c%d8%b9-%d8%ba%d8%b0%d8%a7%db%8c%db%8c-%d8%a8%d9%87%d8%af%d8%a7%d8%b4%d8%aa-1400/">تخمین رتبه ارشد علوم و صنایع غذایی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%81%d9%86%d8%a7%d9%88%d8%b1%db%8c-%d8%a7%d8%b7%d9%84%d8%a7%d8%b9%d8%a7%d8%aa-%d8%b3%d9%84%d8%a7%d9%85%d8%aa-1400/">تخمین رتبه ارشد فناوری اطلاعات سلامت</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d8%aa%d8%b5%d9%88%db%8c%d8%b1%d8%a8%d8%b1%d8%af%d8%a7%d8%b1%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد فناوری تصویربرداری پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%81%db%8c%d8%b2%db%8c%da%a9-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد فیزیک پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%81%db%8c%d8%b2%db%8c%d9%88%d8%aa%d8%b1%d8%a7%d9%be%db%8c-1400/">تخمین رتبه ارشد فیزیوتراپی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%81%db%8c%d8%b2%db%8c%d9%88%d9%84%d9%88%da%98%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد فیزیولوژی پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%82%d8%a7%d8%b1%da%86-%d8%b4%d9%86%d8%a7%d8%b3%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد قارچ شناسی پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%da%a9%d8%a7%d8%b1-%d8%af%d8%b1%d9%85%d8%a7%d9%86%db%8c-1400/">تخمین رتبه ارشد کاردرمانی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%da%a9%d8%aa%d8%a7%d8%a8%d8%af%d8%a7%d8%b1%db%8c-%d9%88-%d8%a7%d8%b7%d9%84%d8%a7%d8%b9-%d8%b1%d8%b3%d8%a7%d9%86%db%8c/">تخمین رتبه ارشد کتابداری و اطلاع رسانی پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%da%a9%d9%86%d8%aa%d8%b1%d9%84-%d9%85%d9%88%d8%a7%d8%af-%d8%ae%d9%88%d8%b1%d8%a7%da%a9%db%8c-1400/">تخمین رتبه ارشد کنترل مواد خوراکی و آشامیدنی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%da%af%d9%81%d8%aa%d8%a7%d8%b1-%d8%af%d8%b1%d9%85%d8%a7%d9%86%db%8c-1400/">تخمین رتبه ارشد گفتار درمانی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%85%d8%a7%d9%85%d8%a7%db%8c%db%8c-1400/">تخمین رتبه ارشد مامایی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%85%d8%af%d8%af%da%a9%d8%a7%d8%b1%db%8c-%d8%a7%d8%ac%d8%aa%d9%85%d8%a7%d8%b9%db%8c-1400/">تخمین رتبه ارشد مددکاری اجتماعی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%85%d8%af%db%8c%d8%b1%db%8c%d8%aa-%d8%aa%d9%88%d8%a7%d9%86%d8%a8%d8%ae%d8%b4%db%8c-1400/">تخمین رتبه ارشد مدیریت توانبخشی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%85%d8%af%db%8c%d8%b1%db%8c%d8%aa-%d8%ae%d8%af%d9%85%d8%a7%d8%aa-%d8%a8%d9%87%d8%af%d8%a7%d8%b4%d8%aa%db%8c-1400/">تخمین رتبه ارشد مدیریت خدمات بهداشتی و درمانی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%85%d8%af%db%8c%d8%b1%db%8c%d8%aa-%d8%b3%d9%84%d8%a7%d9%85%d8%aa%d8%8c-%d8%a7%db%8c%d9%85%d9%86%db%8c-%d9%88-%d9%85/">تخمین رتبه ارشد مدیریت سلامت، ایمنی و محیط زیست (HSE)</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%85%d9%87%d9%86%d8%af%d8%b3%db%8c-%d8%a8%d9%87%d8%af%d8%a7%d8%b4%d8%aa-%d8%ad%d8%b1%d9%81%d9%87-%d8%a7%db%8c-1400/">تخمین رتبه ارشد مهندسی بهداشت حرفه ای</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%85%db%8c%da%a9%d8%b1%d9%88%d8%a8-%d8%b4%d9%86%d8%a7%d8%b3%db%8c-%d8%ba%d8%b0%d8%a7%db%8c%db%8c1400/">تخمین رتبه ارشد میکروب شناسی مواد غذایی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%85%db%8c%da%a9%d8%b1%d9%88%d8%a8-%d8%b4%d9%86%d8%a7%d8%b3%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد میکروبیولوژی پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%86%d8%a7%d9%86%d9%88%d8%aa%da%a9%d9%86%d9%88%d9%84%d9%88%da%98%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد نانوفناوری پزشکی</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%87%d9%85%d8%a7%d8%aa%d9%88%d9%84%d9%88%da%98%db%8c-%d8%ae%d9%88%d9%86-%d8%b4%d9%86%d8%a7%d8%b3%db%8c-1400/">تخمین رتبه ارشد هماتولوژی (خون شناسی)</a></div>
                                <div className={'col-12 col-lg-6 mt-3 p-0'}><a target="_blank" href="https://moshaveranetahsili.ir/%d8%aa%d8%ae%d9%85%db%8c%d9%86-%d8%b1%d8%aa%d8%a8%d9%87-%d8%a7%d8%b1%d8%b4%d8%af-%d9%88%db%8c%d8%b1%d9%88%d8%b3-%d8%b4%d9%86%d8%a7%d8%b3%db%8c-%d9%be%d8%b2%d8%b4%da%a9%db%8c-1400/">تخمین رتبه ارشد ویروس شناسی پزشکی</a></div>

                              </ul>
                            </div>
                        </div>
                </div>
            </div>
        :
            <div></div>
        } */}
        </div>
        <div className={'w-100 d-flex justify-content-center mt-4'}>
            <SamandehiLogo
                 // optional true | false
                sid="249476"
                sp="uiwkaodspfvlaodsjyoegvka"
            />
            <EnamadLogo/>
        </div>

    </div>
}

import React from "react";
export default function Contact(){
    return <div className={'p-4 d-flex flex-column justify-content-start align-items-start'}>
        {/* <img src={'./404.png'} alt=""/> */}
        <h1 className={'pl-5 pr-5'}>انواع راه های ارتباطی با مرکز مشاوران تحصیلی
</h1>
        <p className={'pl-5 pr-5 text-danger'}>“لطفا توجه داشته باشید که به دلیل شرایط کرونا، مرکز مشاوران تحصیلی به هیچ عنوان، مراجع حضوری نمی پذیرد. لطفاً برای مشاوره حضوری به محل دفتر مراجعه نشود. مرکز مشاوران تحصیلی سعی کرده است تا راه های مختلف مشاوره غیر حضوری را برای شما فراهم نماید تا بتواند بهترین پشتیبانی و خدمات را به شما داوطلبان عزیز، ارائه کند.”</p>
    <p className={'pl-5 pr-5 font-weight-bold' }>آدرس:<span className={'text-success'}> تهران، اتوبان رسالت، خیابان فرجام، خیابان مظفری نیا، کوچه اعرابی، پلاک 7، واحد 8</span></p>
    <p className={'pl-5 pr-5 font-weight-bold' }>کدپستی:<span className={'text-success'}> 1687693919</span></p>
    <p className={'pl-5 pr-5 font-weight-bold' }>ساعات کاری مرکز مشاوران تحصیلی:<span className={'text-success'}> 9 الی 19</span></p>
    <p className={'pl-5 pr-5 font-weight-bold' }>تلفن تماس:<span className={'text-success'}> 28424468-021</span></p>
    <p className={'pl-5 pr-5 font-weight-bold' }>تلفن انتقادات و پیشنهادات:<span className={'text-success'}> 77134626-021</span></p>

    <p className={'pl-5 pr-5'}>در صورتی که به هر دلیلی امکان تماس با شماره های ثابت مرکز مشاوران تحصیلی را نداشتید، می توانید از طریق تلگرام نیز با ما در ارتباط باشید:</p>
    <p className={'pl-5 pr-5 font-weight-bold' }>آیدی تلگرام:<span className={'text-success'}>moshaveranetahsiligroup@</span></p>

    <p className={'pl-5 pr-5'}>از طریق فرم زیر نیز می توانید سوالات، انتقادات و پیشنهادات خود را با ما در میان بگذارید.</p>
    </div>
}
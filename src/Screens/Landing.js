import React, {useEffect, useState} from "react";
import heroEntekhab from '../assets/hero-entekhab.png'
import img1 from '../assets/1 (1).jpg'
import img2 from '../assets/3 (1).jpg'
import img3 from '../assets/5 (1).jpg'
import img4 from '../assets/7.jpg'
import icon1 from '../assets/1 icon (1).jpg'
import icon2 from '../assets/2 icon (1).jpg'
import icon3 from '../assets/3 icon (1).jpg'
import icon4 from '../assets/4 icon (1).jpg'
import icon5 from '../assets/5 icon (1).jpg'
import icon6 from '../assets/6 icon (1).jpg'
import SamandehiLogo from "../Components/Samandehi/SamandehiLogo";
import EnamadLogo from "../Components/Samandehi/EnamadLogo";
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import { useHistory } from "react-router-dom";
import { Redirect } from 'react-router-dom';
import Fade from 'react-bootstrap/Fade'


export default function Landing(){
    let history = useHistory();
    const [eventk, setEventk]=useState(null)
    const [open, setOpen] = useState(false);
    const handleSelect = (eventKey) => {
        setEventk(eventKey)
        
    };
    useEffect(()=>{
        if (eventk==='4.1'){
            // return <Redirect to='/نرم-افزار-انتخاب-رشته-علوم-1403' />
            window.open('/نرم-افزار-انتخاب-رشته-ارشد', '_blank')
            setEventk(null)
            // history.push("/نرم-افزار-انتخاب-رشته-علوم-1403");
        }
        if (eventk==='4.2'){
            window.open('/نرم-افزار-تخمین-رتبه-ارشد', '_blank')
            // history.push('/نرم-افزار-تخمین-رتبه-علوم-1403');
            setEventk(null)
        }
        if (eventk==='4.3'){
            window.open('/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت', '_blank')
            // history.push('/نرم-افزار-انتخاب-رشته-بهداشت-1403');
            setEventk(null)
        }
        if (eventk==='4.4'){
            window.open('/تخمین-رتبه-ارشد-وزارت-بهداشت', '_blank')
            // history.push('/نرم-افزار-تخمین-رتبه-بهداشت-1403');
            setEventk(null)
        }
    },[eventk])
    useEffect(()=>{
        setOpen(true)
    },[])
    return <div className={' pb-4 d-flex flex-column container align-items-center'}>
        <div className={'d-flex justify-content-start w-100'}>
            <img className="d-block my-4" src='./logo.png' alt=""/>
        </div>
        <div className={'mb-5 d-flex justify-content-start w-100'}>
        <Nav  onSelect={handleSelect}>
            <NavDropdown title="نرم افزارها" id="nav-dropdown">
                <NavDropdown.Item eventKey="4.1">کمک انتخاب رشته ارشد وزارت علوم </NavDropdown.Item>
                <NavDropdown.Item eventKey="4.2">کمک تخمین رتبه ارشد علوم</NavDropdown.Item>
                <NavDropdown.Item eventKey="4.3">کمک انتخاب رشته ارشد وزارت بهداشت</NavDropdown.Item>
                <NavDropdown.Item eventKey="4.4">کمک تخمین رتبه ارشد بهداشت</NavDropdown.Item>
            </NavDropdown>
            <Nav.Item>
                <Nav.Link eventKey="1" href="/about" target="_blank">
                درباره ما
                </Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link eventKey="2" href="/contact" target="_blank">
                تماس با ما
                </Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link eventKey="3" href="/collaborate" target="_blank">
                همکاری با ما
                </Nav.Link>
            </Nav.Item>
        </Nav>
        </div>
        <div className={'input-box d-flex w-100 mb-5 p-5 flex-column text-center bg-primary text-white'}>
            <h3>نرم افزارهای مشاوریار گروه مشاوران تحصیلی مطمئن ترین، سریع‌ترین و دقیق‌ترین ابزار در خدمت مشاوران تحصیلی در مقاطع تحصیلات تکمیلی کشور</h3>
            <div className={'mt-3 d-flex flex-row flex-wrap justify-content-around'}>
                <div className={' col-12 col-lg-3 p-1 flex-column text-center text-white'}>
                <Fade in={open} timeout='3000'>
                    <h1><i class="bi bi-check-lg ml-4"></i>مطمئن</h1>
                </Fade>
                </div>
                <div className={' col-12 col-lg-3 p-1 flex-column text-center text-white'}>
                <Fade in={open} timeout='3000'>
                    <h1><i class="bi bi-check-lg ml-4"></i> سریع</h1>
                </Fade>
                </div>
                <div className={' col-12 col-lg-3 p-1 flex-column text-center text-white'}>
                <Fade in={open} timeout='3000'>
                    <h1><i class="bi bi-check-lg ml-4"></i> دقیق</h1>
                </Fade>
                </div>
            </div>
        </div>
        
            <div className="row w-100 justify-content-center">
                <div className="pt-4 pt-lg-0  d-flex flex-column">
                        <div className={'input-box p-5 mb-5'}>
                            
                            <div className=" mx-auto d-flex flex-wrap flex-row">
                                <div className={'col-lg-4'}> 
                                <img src={img1} className="w-100" alt=""/>
                                </div>
                                <div className={'col-lg-8'}>
                                <ul className={'d-flex flex-wrap list-unstyled'}>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>آنالیز دقیق شانس قبولی با دقتی بیش از 98 درصد براساس داده پردازی های سهمیه و رتبه کارنامه های ارشد وزارت علوم</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>تحلیل آماری و مقایسه ای شانس قبولی در 4 سطح قطعی، پراحتمال، خوشبینانه و کم احتمال</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>آنالیز تک تک کد رشته دانشگاه ها در رشته های تحت پوشش به کمک متخصصان داده پردازی و کارشناسان آموزشی</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>جستجوی آسان براساس نوع دوره دانشگاهی، استان ها و رشته-گرایش</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>اولویت بندی صحیح بر مبنای الگوریتم های پشتیبان تصمیم گیری در 3 متغیر اعتبار، کیفیت و رنگینگ دانشگاه  ها</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>اولویت بندی براساس پردازش متغیرهای کیفی محبوبیت گرایش، بازار کار و امکان ادامه تحصیل</li>
                                </ul> 
                                </div>
                                
                            </div>
                            <a href='/نرم-افزار-انتخاب-رشته-ارشد' target="_blank"><p className={'text-right ml-5 '}><u>ورود به نرم افزار</u></p></a>
                        </div>
                        
                </div>
            </div>
            <div className="row w-100 justify-content-center">
                <div className="pt-4 pt-lg-0  d-flex flex-column">
                        <div className={'input-box p-5 mb-5'}>
                            
                            <div className=" mx-auto d-flex flex-wrap flex-row">
                                <div className={'col-lg-4'}> 
                                <img src={img2} className="w-100" alt=""/>
                                </div>
                                <div className={'col-lg-8'}>
                                <ul className={'d-flex flex-wrap list-unstyled'}>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>آنالیز تمامی رتبه ها و ترازهای تأثیرگذار در نمودارهای لگاریتمیک رتبه تراز سازمان سنجش</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>اثردهی تاثیر معدل موثر و نمره خام علمی و رساندن دقت نرم افزار به بیش از 95 درصد</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>تحلیل جداگانه تأثیرگذاری سهمه های ایثارگری بر روی رتبه های کشوری در مجموع سهمیه ها</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>سرعت میلی ثانیه ای داده پردازی نرم افزار به ازای هر گرایش هر رشته</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>داده پردازی تخصصی توسط تیم های آماری و تست و تأیید نهایی توسط اساتید رشته های تحت پوشش</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>پوشش دهی بیش از 70 درصد رشته گرایش های کارشناسی ارشد وزارت علوم</li>
                                </ul>
                                </div>
                                
                            </div>
                            <a href='/نرم-افزار-تخمین-رتبه-ارشد' target="_blank"><p className={'text-right ml-5 '}><u>ورود به نرم افزار</u></p></a>
                        </div>
                </div>
            </div>
            <div className="row w-100 justify-content-center">
                <div className="pt-4 pt-lg-0  d-flex flex-column">
                        <div className={'input-box p-5 mb-5'}>
                            
                            <div className=" mx-auto d-flex flex-wrap flex-row">
                                <div className={'col-lg-4'}> 
                                <img src={img3} className="w-100" alt=""/>
                                </div>
                                <div className={'col-lg-8'}>
                                <ul className={'d-flex flex-wrap list-unstyled'}>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>تنها نرم افزار موجود در حوزه تحلیل داده های کارشناسی ارشد وزارت بهداشت</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>آنالیز دقیق شانس قبولی با دقتی بیش از 98 درصد براساس داده پردازی های سهمیه و رتبه کارنامه های ارشد وزارت بهداشت</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>تحلیل آماری و مقایسه ای شانس قبولی در 4 سطح قطعی، پراحتمال، خوشبینانه و کم احتمال</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>پوشش دهی تمامی رشته های کارشناسی ارشد وزارت بهداشت و تحلیل تک تک کد رشته دانشگاه ها به کمک متخصصان داده پردازی و کارشناسان آموزشی</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>رابط کاربری بسیار ساده و سریع</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>اولویت بندی صحیح بر مبنای الگوریتم های پشتیبان تصمیم گیری در 3 متغیر اعتبار، کیفیت و رنگینگ دانشگاه ها</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>اولویت بندی دقیق براساس متغیرهای کیفی بازار کار و امکان ادامه تحصیل</li>
                                </ul>
                                </div>
                                
                            </div>
                            <a href='/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت' target="_blank"><p className={'text-right ml-5 '}><u>ورود به نرم افزار</u></p></a>
                        </div>
                </div>
            </div>
            <div className="row w-100 h-100 justify-content-center">
                <div className="pt-4 pt-lg-0  d-flex flex-column">
                        <div className={'input-box p-5 mb-5'}>
                            
                            <div className=" mx-auto d-flex flex-wrap flex-row">
                                <div className={'col-lg-4'}> 
                                <img src={img4} className="w-100" alt=""/>
                                </div>
                                <div className={'col-lg-8'}>
                                <ul className={'d-flex flex-wrap list-unstyled'}>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>داده پردازی  و تحلیل آماری حداقل نیمی از رتبه ها و ترازهای کارنامه های ارشد وزارت بهداشت</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>دقت بیش از 90 درصدی نرم افزار</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>آنالیز تأثیر بیست درصدی معدل براساس ضریب معدل موثر</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>سرعت میلی ثانیه ای نرم افزار در ارائه نتیجه آنالیز درصدها و معدل</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>داده پردازی توسط تیم های تخصصی آماری و تأیید نهایی آنها توسط اساتید هر رشته</li>
                                <li className={'col-12 mt-3 p-0'}><i class="bi bi-check-lg ml-2"></i>پوشش دهی تمامی رشته های ارشد وزارت بهداشت</li>
                                </ul>
                                </div>
                                
                            </div>
                            <a href='/تخمین-رتبه-ارشد-وزارت-بهداشت' target="_blank"><p className={'text-right ml-5 '}><u>ورود به نرم افزار</u></p></a>
                        </div>
                </div>
            </div>

            <div className={'input-box d-flex w-100 mb-5 p-5 flex-column text-center bg-primary text-white'}>
                
                <div className={'d-flex flex-row flex-wrap justify-content-around'}>
                    <div className={' col-12 col-lg-3 p-1 flex-column text-center text-white'}>
                        <h5>تعداد پردازش‌های نرم افزاری</h5>
                        <h1>150,000+</h1>
                    </div>
                    <div className={' col-12 col-lg-3 p-1 flex-column text-center text-white'}>
                        <h5>تعداد مشاوران استفاده کننده</h5>
                        <h1>200+</h1>
                    </div>
                    <div className={' col-12 col-lg-3 p-1 flex-column text-center text-white'}>
                        <h5>متخصصان تحلیل داده</h5>
                        <h1>40+</h1>
                    </div>
                </div>
                <div className={'d-flex flex-row flex-wrap justify-content-around'}>
                    <div className={' col-12 col-lg-3 p-1 flex-column text-center text-white'}>
                        
                    </div>
                    <div className={' col-12 col-lg-3 p-1 flex-column text-center text-white'}>
                        
                    </div>
                    <div className={' col-12 col-lg-3 p-1 flex-column text-center text-white'}>
                        
                    </div>
                </div>
            </div>

            <div className={'input-box d-flex w-100 mb-5 p-5 flex-column text-center bg-white text-black'}>
                <h2 className={'mb-4 text-black'}>آنچه که ما را بی‌رقیب و قابل اعتماد می‌کند</h2>
                <div className={'d-flex flex-row flex-wrap justify-content-around'}>
                    <div className={' col-12 col-lg-4 p-1 flex-column text-center text-black mb-2'}>
                        <img src={icon1} className="w-50 h-50 mb-2" alt=""/>
                        <h5 >بالاترین دقت</h5>
                    </div>
                    <div className={' col-12 col-lg-4 p-1 flex-column text-center text-black mb-2'}>
                        <img src={icon2} className="w-50 h-50 mb-2" alt=""/>
                        <h5>بیشترین صحت</h5>
                    </div>
                    <div className={' col-12 col-lg-4 p-1 flex-column text-center text-black mb-2'}>
                        <img src={icon3} className="w-50 h-50 mb-2" alt=""/>
                        <h5>کاربری سریع و آسان</h5>
                    </div>
                    <div className={' col-12 col-lg-4 p-1 flex-column text-center text-black mb-2'}>
                        <img src={icon4} className="w-50 h-50 mb-2" alt=""/>
                        <h5 >پشتیبانی کامل و متعهدانه</h5>
                    </div>
                    <div className={' col-12 col-lg-4 p-1 flex-column text-center text-black mb-2'}>
                        <img src={icon5} className="w-50 h-50 mb-2" alt=""/>
                        <h5>تحلیل دقیق تک تک داده ها</h5>
                    </div>
                    <div className={' col-12 col-lg-4 p-1 flex-column text-center text-black mb-2'}>
                        <img src={icon6} className="w-50 h-50 mb-2" alt=""/>
                        <h5>منحصر به فرد و بی‌رقیب</h5>
                    </div>
                </div>
            </div>
            <div className={'w-100 d-flex justify-content-center mt-4'}>
                <SamandehiLogo
                    // optional true | false
                    sid="249476"
                    sp="uiwkaodspfvlaodsjyoegvka"
                />
                <EnamadLogo/>
            </div>
    </div>
}
import React, {useEffect, useState} from 'react'
import AdminRequireLoginMiddleware from "../../AdminRequireLoginMiddleware";
import useApi from "../../useApi/useApi";
import {postProcessAdmin, preProcessAdmin} from "../../useApi/preProcesses/AdminProcessApi";
import SpinnerLoading from "../../Components/Spinner";
import {Modal} from "react-bootstrap";
import {Line} from 'react-chartjs-2';
import {fixPersianNumbers} from '../../HelperFunction'
import Select from "../../Components/Select";
import cogoToast from 'cogo-toast';
import InputNumber from "../../Components/InputNumber";
import { CSVLink } from "react-csv";
import {postProcessUser, preProcessUser} from "../../useApi/preProcesses/UserProcesseApi";

export default function AdminChart() {
    const [data, setData] = useState([])
    const [id, setId] = useState(null)
    const [inputPrice, setInputPrice] = useState(null)
    const [groups, setGroups] = useState([])
    const [filterFieldList, setFilterFieldList] = useState([])
    const [filterTendencyList, setFilterTendencyList] = useState([])
    const [getData, setGetData] = useState(false)
    const [setPrice, setSetPrice] = useState(false)
    const [showModal, setShowModal] = useState(false)
    const [showModalDelete, setShowModalDelete] = useState(false)
    const [selectedRow, setSelectedRow] = useState({id: null, level: null, rank: null, nationalRank: null})
    const [deleteActive, setDeleteActive] = useState(false)
    const [filterGroup, setFilterGroup] = useState('')
    const [filterField, setFilterField] = useState('')
    const [filterTendency, setFilterTendency] = useState('')
    const [validation, setValidation] = useState({level: false, rank: false, nationalRank: false})
    const [counter, setCounter] = useState(1)
    const [deleteId, setDeleteId] = useState(null)
    const quotaOptions = [{id: "1", name: 'آزاد'}, {id: "2", name: '۵ درصد'}]

    console.log(filterTendency)
    const [groupsData, groupsStatus] = useApi(
        preProcessAdmin('groups', {}),
        postProcessAdmin, [],
        true);

    const [filterFieldData, filterFieldStatus] = useApi(
        preProcessUser('fieldsChoice', {group: filterGroup}),
        postProcessUser, [filterGroup],
        filterGroup);


    const [chartData, chartStatus] = useApi(
        preProcessAdmin('price', {id: filterField}),
        postProcessAdmin, [filterField, counter], filterField);

    const [setPriceRes, setPriceResStatus] = useApi(
        preProcessAdmin('set-price', inputPrice),
        postProcessAdmin, [setPrice], setPrice);

    const [setDeleteRes, setDeleteResStatus] = useApi(
        preProcessAdmin('delete-price', {"id": deleteId}),
        postProcessAdmin, [deleteId], deleteId);


    useEffect(() => {
        if (groupsStatus === 'SUCCESS') {
            setGroups(groupsData.list)
        }
    }, [groupsStatus])

    useEffect(() => {
        if (filterFieldStatus === 'SUCCESS') {
            setFilterFieldList(filterFieldData.list)
        }
    }, [filterFieldStatus])

    useEffect(() => {
        if (chartStatus === 'SUCCESS') {
            setData(chartData.list)
        }
        setGetData(false)
    }, [chartStatus])

    useEffect(() => {
        if (setPriceResStatus === 'SUCCESS') {
            setCounter(counter + 1)
            cogoToast.success('عملیات ذخیره با موفقیت انجام شد');
        }
    }, [setPriceResStatus])

    useEffect(() => {
        if (setDeleteResStatus === 'SUCCESS') {
            setInputPrice(null)
            setData([])
            setCounter(counter + 1)
            cogoToast.success('عملیات حذف با موفقیت انجام شد');
        }
    }, [setDeleteResStatus])


    useEffect(() => {
        setInputPrice(null)
        setFilterFieldList([])
        setFilterTendencyList([])
        setFilterField(null)
        setFilterTendency(null)
        setData([])
    }, [filterGroup])

    useEffect(() => {
        setInputPrice(null)
        setData([])
    }, [filterField])

    function hideModal() {
        setShowModal(false)
        setSelectedRow({id: null, level: null, rank: null, nationalRank: null})
    }

    function hideModalDelete() {
        setSelectedRow({id: null, level: null, rank: null, nationalRank: null})
        setShowModalDelete(false)
    }


    return AdminRequireLoginMiddleware(<div>
        <SpinnerLoading
            show={[chartStatus,  groupsStatus, filterFieldStatus].includes('LOADING')}/>
        <div className={'container pt-5'}>
            <div className={'d-flex justify-content-between'}>
                <div className={'card d-flex flex-column align-items-center col-12 col-lg-4 py-2'}>

                    <Select className={'mb-2'} placeHolder={'انتخاب گروه'} options={groups} value={filterGroup}
                            onChange={value => setFilterGroup(value)}/>
                    {filterFieldList.length > 0 &&
                    <Select className={'mb-2'} placeHolder={'انتخاب رشته'} options={filterFieldList}
                            onChange={value => setFilterField(value)}/>}
                </div>
            </div>
            {data.length > 0 && <div>
                <div className={'mt-4 table-responsive  table-level-rank'}>
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">name</th>
                            <th scope="col">price</th>
                            {/* <th scope="col">گروه</th>
                            <th scope="col">رشته</th> */}
                            <th scope="col">عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        {data && data.map((item, index) => {
                            return <tr key={index}>
                                <th scope="row">{index + 1}</th>
                                <td>{item.name}</td>
                                <td>
                                <input 
                                value={data[index].price}
                                onChange={(e) => {
                                    let temp = {
                                        id: item.id,
                                        price:Number(fixPersianNumbers(e.target.value)),
                                        pack: {id: item.pack.id},
                                        field: {id: item.field.id}
                                    }
                                    data[index].price = temp.price
                                    setInputPrice(temp)
                                }}
                                /></td>
                                {/* <td>{item.field?.group?.name}</td>
                                    <td>{item.field?.name}</td> */}
                                    <td>
                                        <button className={'btn btn-primary'}
                                                onClick={() => setSetPrice(item.price)}>ذخیره
                                        </button>
                                    </td>
                                    <td>
                                        <button className={'btn btn-danger'}
                                                onClick={() => item.id && setDeleteId(item.id)}>حذف
                                        </button>
                                    </td>
                                </tr>
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>}
                <Modal show={showModalDelete} onHide={hideModalDelete}>
                    <Modal.Header>
                        <Modal.Title>حذف تراز و رتبه</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Enter price below:</p>
                        <input />
                        <button className={'btn btn-primary mt-3'} onClick={() => {
                            setDeleteActive(true)
                        }}>بلی
                        </button>
                        <button className={'btn btn-secondary mt-3 mx-2'} type={'button'}
                                onClick={hideModalDelete}>خیر
                        </button>
                    </Modal.Body>
                </Modal>
            </div>
        </div>
    )
}

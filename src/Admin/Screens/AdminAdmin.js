import React, {useEffect, useState} from 'react'
import AdminRequireLoginMiddleware from "../../AdminRequireLoginMiddleware";
import useApi from "../../useApi/useApi";
import {preProcessAdmin, postProcessAdmin} from "../../useApi/preProcesses/AdminProcessApi";
import SpinnerLoading from "../../Components/Spinner";
import cogoToast from 'cogo-toast';
export default function AdminAdmin() {
    const [value, setValue] = useState(false)
    const [id, setId] = useState(null)
    const [save, setSave] = useState(false)

    const [groupsData, groupsStatus] = useApi(
        preProcessAdmin('admin', {}),
        postProcessAdmin, [],
        true);
        console.log(groupsData)
        console.log(id,value)

    const [saveData, saveStatus] = useApi(
        preProcessAdmin('save', {id, value}),
        postProcessAdmin, [save],
        save);

    useEffect(() => {
        if (saveStatus === 'SUCCESS') {
                    cogoToast.success('عملیات آپلود با موفقیت انجام شد');
                } else if (saveStatus === 'ERROR') {
                    cogoToast.error('عملیات آپلود با خطا مواجه شد');
                }
        setSave(false)
        setId(null)
    }, [saveStatus])

    // useEffect(() => {
    //     if (uploadStatus === 'SUCCESS') {
    //         cogoToast.success('عملیات آپلود با موفقیت انجام شد');
    //     } else if (uploadStatus === 'ERROR') {
    //         cogoToast.error('عملیات آپلود با خطا مواجه شد');
    //     }
    //     setTypeFile(null)
    //     setUploadActive(false)
    // }, [uploadStatus])


    return AdminRequireLoginMiddleware(
        <div>
            {/* <SpinnerLoading show={uploadStatus === 'LOADING' || groupsStatus === 'LOADING'}/> */}
            <div className={'container d-flex justify-content-center pt-5'}>
                <div className={'col-12 col-lg-7'}>
                {Object.keys(groupsData).length && <div>
                <div className={'mt-4 table-responsive  table-level-rank'}>
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">name</th>
                            <th scope="col">value</th>
                        </tr>
                        </thead>
                        <tbody>
                        {groupsData && groupsData.list.map((item, index) => {
                            return <tr key={index}>
                                <th scope="row">{index + 1}</th>
                                <td>{item.group.name}</td>
                                <td>
                                <input 
                                defaultValue={item.value}
                                onChange={(e) => {
                                    setId(item.id)
                                    setValue(e.target.value)
                                }}
                                /></td>
                                    <td>
                                        <button className={'btn btn-primary'}
                                                onClick={() => setSave(true)}>ذخیره
                                        </button>
                                    </td>
                                </tr>
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>}

                </div>
            </div>
        </div>
    )
}

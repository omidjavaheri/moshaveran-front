import React, {useReducer, useEffect, useState} from 'react';
import {
    BrowserRouter as Router,
    Route, useLocation,Switch,useHistory, Redirect
} from "react-router-dom";
import Home from "./Screens/Home";
import AuthReducer from './Storage/Reducers/AuthReducer';
import AuthContext from './Storage/Contexts/AuthContext';
import Admin from "./Admin/Screens/Admin";
import Store from "./Storage/Store";
import NotFound from "./Screens/NotFound";
import Landing from './Screens/Landing';
import About from './Screens/About';
import Contact from './Screens/Contact';
import Collaborate from './Screens/Collaborate';
import Entekhab from "./Screens/entekhab";
import Level from './Level';
import './App.css'
const initializeUser = {
    user: null,
    apiToken: '',
};

export default function App() {
    const [auth, authDispatch] = useReducer(AuthReducer, {});
    const [year,setYear] = useState(null)
    let years =[1400,1403, 1401, 1403];//برای هر سال به این آرایه اضافه می کنیم
    const [path,setPath] = useState();
    let history = useHistory();
    console.log(window.location.pathname)
    console.log(path)
    const retrieveAuthData = async () => {
        try {
            let result = await Store.get('USER_INFO');
            result = result !== false ? result : initializeUser;
            return result;
        } catch (error) {
        }
    };

    useEffect(() => {
        // if(window.location.pathname==='/aaa'){
        //     return <Redirect from='/aaa' to='/نرم-افزار-انتخاب-رشته-ارشد-1403' />
        // }
        let temp1 =[]
        let temp2 =[]
        let temp3 =[]
        let temp4 =[]
        // let url1 ='/نرم-افزار-تخمین-رتبه-بهداشت-'+item;
        //     let url2 ='/نرم-افزار-تخمین-رتبه-علوم-'+item;
        //     let url3 ='/نرم-افزار-انتخاب-رشته-بهداشت-'+item;
        //     let url4 ='/نرم-افزار-انتخاب-رشته-علوم-'+item;
        years.forEach(item=>{
            let url1 ='/تخمین-رتبه-ارشد-وزارت-بهداشت';
            let url2 ='/نرم-افزار-تخمین-رتبه-ارشد';
            let url3 ='/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت';
            let url4 ='/نرم-افزار-انتخاب-رشته-ارشد';
            // if ([url1,url2].includes(decodeURI(window.location.pathname))){
            //     setYear(item)
            // }
            if (decodeURI(window.location.pathname).search(url3) !== -1 || decodeURI(window.location.pathname).search(url4) !== -1){
                setYear(item)
            }
            temp1.push(url1);
            temp2.push(url2);
            temp3.push(url3);
            temp4.push(url4);
        });
        setPath([temp1,temp2,temp3,temp4])

        retrieveAuthData().then((data) => {
            authDispatch({
                type: 'INIT_DATA',
                data: data,
            });
        });
    }, [])
    return auth.apiToken !== undefined ? <AuthContext.Provider value={{auth, authDispatch}}>
        <Router>
            <Switch>
            <Redirect from='/نرم-افزار-انتخاب-رشته-علوم-1400' to='/نرم-افزار-انتخاب-رشته-ارشد' />
            <Redirect from='/نرم-افزار-انتخاب-رشته-بهداشت-1400' to='/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت' />
            <Redirect from='/نرم-افزار-انتخاب-رشته-علوم-1403' to='/نرم-افزار-انتخاب-رشته-ارشد' />
            <Redirect from='/نرم-افزار-انتخاب-رشته-ارشد-1403' to='/نرم-افزار-انتخاب-رشته-ارشد' />
            <Redirect from='/نرم-افزار-انتخاب-رشته-بهداشت-1403' to='/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت' />
            <Redirect from='/نرم-افزار-انتخاب-رشته-علوم-1399' to='/نرم-افزار-انتخاب-رشته-ارشد' />
            <Redirect from='/نرم-افزار-انتخاب-رشته-بهداشت-1399' to='/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت' />

            <Redirect from='/نرم-افزار-تخمین-رتبه-علوم-1400' to='/نرم-افزار-تخمین-رتبه-ارشد' />
            <Redirect from='/نرم-افزار-تخمین-رتبه-بهداشت-1400' to='/تخمین-رتبه-ارشد-وزارت-بهداشت' />
            <Redirect from='/نرم-افزار-تخمین-رتبه-علوم-1403' to='/نرم-افزار-تخمین-رتبه-ارشد' />
            <Redirect from='/نرم-افزار-تخمین-رتبه-بهداشت-1403' to='/تخمین-رتبه-ارشد-وزارت-بهداشت' />
            <Redirect from='/نرم-افزار-تخمین-رتبه-علوم-1399' to='/نرم-افزار-تخمین-رتبه-ارشد' />
            <Redirect from='/نرم-افزار-تخمین-رتبه-بهداشت-1399' to='/تخمین-رتبه-ارشد-وزارت-بهداشت'/>
                <Route path={path[0]}><Home group={1} year={1403}/></Route>
                <Route path={path[1]}><Home group={2} year={1403}/></Route>
                {/* <Route path={path[2]}><Entekhab group={1} year={year} url={path[2].filter(item=>item.search(year) !== -1)[0]}/></Route> */}
                <Route path={'/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت'}><Entekhab group={1} year={1403} url={'/نرم-افزار-انتخاب-رشته-ارشد-وزارت-بهداشت'}/></Route>
                {/* <Route path={path[3]}><Entekhab group={2} year={year} url={path[3].filter(item=>item.search(year) !== -1)[0]}/></Route> */}
                <Route path={'/نرم-افزار-انتخاب-رشته-ارشد'}><Entekhab group={2} year={1403} url={'/نرم-افزار-انتخاب-رشته-ارشد'}/></Route>
                
                <Route path="/zinc"><Admin/></Route>
                <Route path="/about"><About/></Route>
                <Route path="/contact"><Contact/></Route>
                {/* <Route path="/level"><Level/></Route> */}
                <Route path="/collaborate"><Collaborate/></Route>
                <Route path=""><Landing/></Route>
            </Switch>
        </Router>
    </AuthContext.Provider> : null
}

import React from "react";
import enamdLogo from './enamd.jpg'
interface PropTypes {
    verified?: boolean;
    sid: string | number;
    sp: string;
}

const EnamadLogo = ({
    verified,
    alt,
    onClick,
    ...props
}: PropTypes &
    React.DetailedHTMLProps<
        React.ImgHTMLAttributes<HTMLImageElement>,
        HTMLImageElement
        >) => {

    return (
        <div id="enamad-logo">
            <a referrerPolicy='origin' target='_blank' href='https://trustseal.enamad.ir/?id=455962&amp;Code=fjZzju0WF4VO1PJOcoXXFqGXkIxVoEYR'><img referrerPolicy='origin' src='./enamad.jpg' alt=''/></a>
        </div>
    );
};

EnamadLogo.defaultProps = {
    verified: true,
    width: 150,
    height: 150,
    style: { cursor: "pointer" , padding : 0 },
};

export default EnamadLogo;
